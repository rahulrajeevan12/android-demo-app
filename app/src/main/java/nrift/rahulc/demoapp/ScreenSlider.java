package nrift.rahulc.demoapp;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.ImageView;


public class ScreenSlider extends FragmentActivity {
    private ViewPager mPager;
    private static final int NUM_PAGES = 4;


    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slider);
        try {
            getActionBar().hide();
        }catch(NullPointerException n){

        }

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_screen_slider, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {

            super.onBackPressed();
        } else {

            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }
    public static class ScreenSlidePageFragment extends android.support.v4.app.Fragment {
        private int position;
        public static ScreenSlidePageFragment newInstance(int position) {

            ScreenSlidePageFragment f = new ScreenSlidePageFragment();
            Bundle b = new Bundle();
            b.putInt("position", position);

            f.setArguments(b);

            return f;
        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(
                    R.layout.viewpager_layout, container, false);
            ImageView myImage = (ImageView)rootView.findViewById(R.id.myImage);
            Button myButton = (Button)rootView.findViewById(R.id.textView);
            if(getArguments().getInt("position") == 1){
                myImage.setImageDrawable(getResources().getDrawable(R.drawable.s1));
                myButton.setText("Demo1");

            }
            if(getArguments().getInt("position") == 2){
                myImage.setImageDrawable(getResources().getDrawable(R.drawable.s2));
                myButton.setText("Demo 2");

            }

            if(getArguments().getInt("position") == 3){
                myImage.setImageDrawable(getResources().getDrawable(R.drawable.s5));
                myButton.setText("Finish Demo");
                myButton.setEnabled(true);
                myButton.setBackground(getResources().getDrawable(R.drawable.outline));
                myButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getActivity(),MainActivity.class));
                    }
                });

            }

            if(getArguments().getInt("position") == 0){
                myImage.setImageDrawable(getResources().getDrawable(R.drawable.s5));
                myButton.setText("Demo App");
            }
            return rootView;
        }
    }
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            ScreenSlidePageFragment myfrag = ScreenSlidePageFragment.newInstance(position);
            return myfrag;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

}
